# exercicio_07

Banco de dados local.

Esse projeto mostra um exemplo de utilização de um banco de dados local através da adição de usuários de forma randômica e salvando os usuários novos criados e se estão bloqueados. Dessa forma, mesmo que você feche o aplicativo, os usuários estão salvos locais e você pode vê-los novamente ao abrir o aplicativo.

# Tarefa 01

Adicione uma imagem de perfil atrelada a cada usuário e, para cada elemento da lista, um botão que abra um ImagePicker (https://pub.dev/packages/image_picker) para escolher uma imagem. Após uma imagem ser escolhida, essa deve aparecer na imagem de perfil do usuário.

# Tarefa 02

Salve a imagem escolhida no banco local de forma que está sempre apareça automáticamente quando o app for reiniciado. Para isso, adicione um novo campo no banco já criado.